## Linux mount command explained
#linux #filesystem #mount #unmount #devices

# Theory...what you actually need to know...
It is important to make a remark: "Everything in Linux is a file" (even cd command).\
All the files from a machine/pc are grouped in directories a.k.a. folders. All these directories form a **File System**.\
Therefore, a **file system**
represents a structure of files, which are wrapped in
directories. There are many types of
file systems, one of them is **UFS (Unix File System)** used
for installing Linux (where the root directory is /). In the image below you can see a Linux File System.

![alt text](https://www.google.com/url?sa=i&url=https%3A%2F%2Fdev.to%2Fkcdchennai%2Flinux-file-system-4idj&psig=AOvVaw1XM_bmErYJJ-3QeLvboGTX&ust=1665081196455000&source=images&cd=vfe&ved=0CAwQjRxqFwoTCNj1soHdyfoCFQAAAAAdAAAAABAJ)

**Mounting** a file system, in our case a USB
(because this one has its own file system), **creates a binding**
between the USB file system and the Linux file system
for the duration of the mount. \
Actually, the binding is made
between a directory that is already in the file system hierarchy,
called the **mount point (Linux side)**, and the entry point into the
file system about to be mounted, called the **root of this file system (USB side)**.\
The mount point directory and the root are connected until unmount time.
**When a file system is mounted on a mount point, it overlays the contents of the
mount point directory**. Files, symbolic links, and
subdirectories within the mount point directory are no longer
accessible and are hidden until the file system is unmounted.


_Ok, BUT what does `mount` mean/do in Linux?_\
Well, if you have a version of Linux without a User Interface or if you do not want to use a User Interface
and you want to access the data from a USB device, then you'll
need to use mount command.\
When you plug in a USB device in your machine, a new block device will be added under /dev (which stand for devices).\
If you want to see the data within the USB device or if you want to add some new data to that USB device you'll have to mount the USB file system first.

# Commands...what you actually need to remember...
```bash
# list the devices attached to your machine
sudo fsdisk -l 
# create the mount point
mkdir /location/of/the/mount-point
# mount USB device
mount /dev/sdc1 /location/of/the/mount-point
```

# Practice...what you actually need to do...
```bash
sudo fsdisk -l 
mkdir from-usb
mount /dev/sdc1 /from-usb
```

# Conclusion...what you actually need to understand
* for accessing and adding data to a USB device you need to use `mount` command
* during the mount time, the content of mount-point directory is overlaid

# Bonus
For unmount use the following command:
```bash
umount /location/of/the/mount-point
```