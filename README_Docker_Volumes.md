# utility



## Docker volumes explained
#docker #volumes #containers #storage 

# Theory...what you actually need to know...
When a Docker container is deleted or restarted all the data are lost :slightly_frowning_face: \
In order to avoid this to happen, Docker provides 4 types of storage:
* **volumes** > recommended way > created and managed by Docker
* mount bind > have limited functionality compared to volumes > a file or directory on the host machine is mounted into a container
* tmpfs mounts > not persisted on disk, Docker host or within a container > store non-persistent state or sensitive information during a container lifetime
* named pipes > used for communication between the Docker host and a container

Volumes can be of 3 types:
* host volumes > store sth from Docker host to a Docker container
* anonymous volumes > store sth in Docker container > the location of volume is managed by Docker
* **named volumes** > store sth in Docker container, but you can give it a name > the location of volume is managed by Docker

**Best Practice:** when you want to **store data** even if the container is deleted/restarted use **Named Docker Volumes**.

# Commands...what you actually need to remember...
```yaml
docker volume create volume-name 
docker run -d --name app-name -v volume-name:/path/in/container image-name:tag
```

The first command above will create a Docker Named Volume with name volume-name.\
The second command above will do more things:
* if on the host there is no image-name:tag > it'll pull it from Docker Hub, otherwise this step will be skipped
* it'll create/run a Docker container from image-name:tag image
* it'll attach volume-name to app-name container

# Practice...what you actually need to do...
```yaml
docker volume create my-vol 
docker run -d --name my-app -v my-vol:/app nginx:latest
```

# Conclusion...what you actually need to understand
* when a container disappear (deleted/restarted) all data are lost
* to avoid this, stoare relevant data to a persistant stoarage
* best way is to use a Docker Named Volume
* for example, if your container needs some data from a file, save that file in a volume > the file will remain there even if the container disappears
